package ir.redbees.reactbooks.modules.book.Dao;

import ir.redbees.reactbooks.modules.book.Entity.DBFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoverRepository extends JpaRepository<DBFile, String> {
}
